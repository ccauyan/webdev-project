<?php

	include_once('../includephps/dbconnect.php');
	include_once('../includephps/entry.php');  // not sure if this is still needed, will put in just in case

	// gets data from form
	$output = '';

	//print_r($_POST);

	session_start();
	if (isset($_POST['search'])) 
	{
		$searchq = filter_input(INPUT_POST, 'search', FILTER_SANITIZE_FULL_SPECIAL_CHARS); //removed sanitization to isolate issue
		//$searchq = preg_replace("#[^0-9a-z]#i", "", $searchq);

		$query = $pdo->prepare("SELECT * FROM candidates WHERE name LIKE (:searchq)");
		// $query = $pdo->prepare("SELECT * FROM candidates WHERE name = ?");
		$query->bindValue(':searchq', $searchq, PDO::PARAM_STR);
		$query->execute();

		$num = $query->rowCount();
		if ($num == 0) 
		{
			$output = 'No search results.';
			// print_r($searchq);
			print_r($query);
		}

		else
		{
			while ($row = $query->fetch(PDO::FETCH_ASSOC))
			{
				$name = $row['name'];
				$details = $row['details'];
				$date = date('l jS', $row['entrydatetime']);
				$output .= '<div> '.$name.'<br><br>'.$details.'<br><br>'.$date.'</div>'.'<br>';
				
			}


		}
	}



?>

<!DOCTYPE html>
<html>
<head>
	<title>Search</title>
	<link rel="stylesheet" type="text/css" href="../stylescss/style.css">
</head>
<body>

	<form action="index.php" method="POST">
		<input type="text" name="search" placeholder="Search for something" />
		<input type="submit" value=">>" />

	</form>

	<?php print("$output"); ?>
</body>
</html>