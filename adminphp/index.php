<?php

	session_start();
	include_once('../includephps/dbconnect.php');

	if (isset($_SESSION['loggedin'])) 
	{
		// will display index.php if logged in
		print_r($_POST);

		 ?>

		<!DOCTYPE html>
		<html lang="en">
		<head>
			<title>Candidate Scheduler CMS</title>
			<link rel="stylesheet" type="text/css" href="../stylescss/style.css">
		</head>
		<body>
			<div class="container">
				<a href="index.php" id="logo">
					Candidate Scheduler
				</a>

				<br>
				
				<ol>
					<li>
						<a href="add.php">Add Candidate</a>
					</li>
					<li>
						<a href="delete.php">Remove Candidate</a>
					</li>
					<li>
						<a href="logout.php">Logout</a>
					</li>
				</ol>

			</div>

		</body>
		</html>

		 <?php
	}
	else
	{
		// will display login banner
		if (isset($_POST['username'], $_POST['password'])) 
		{
			$username = $_POST['username'];
			$password = md5($_POST['password']);

			// checks if empty field just in case
			if (empty($username) OR empty($password)) 
			{
				$errormsg = 'Error! All fields are required.';
			}
			else
			{
				$query = $pdo->prepare("SELECT * FROM userlogins WHERE username = ? AND password = ?");

				$query->bindValue(1, $username);
				$query->bindValue(2, $password);

				$query->execute();

				$num = $query->rowCount();

				print_r($query);

				if ($num == 1) 
				{
					// to really double check that the user entered the correct credentials
					$_SESSION['loggedin'] = true; // remembers user
					header('Location: index.php');
					exit();
				}
				else
				{
					// this means the user entered wrong credentials
					$errormsg = 'Incorrect username and/or password';
				}
			}
		}

		?>

		<!DOCTYPE html>
		<html lang="en">
		<head>
			<title>Candidate Scheduler CMS</title>
			<link rel="stylesheet" type="text/css" href="../stylescss/style.css">
		</head>
		<body>
			<div class="container">
				<a href="index.php" id="logo">
					Candidate Scheduler
				</a>

				<br>

				<?php if (isset($errormsg)) { ?>
					<small style="color: red;">
						<?php echo $errormsg; ?>
					</small>
				<?php } ?>

				<br>
				<form action="index.php" method="POST" autocomplete="OFF">
					<input type="text" name="username" placeholder="username" />
					<input type="text" name="password" placeholder="password" />
					<input type="submit" value="Login" />
				</form>

			</div>

			</body>
			</html>

		<?php
	}

?>