<?php

	session_start();
	include_once('../includephps/dbconnect.php');

	if (isset($_SESSION['loggedin']))
	{
		print_r($_POST);
		// user can add candidate entry
		if (isset($_POST['name'], $_POST['details'])) 
		{
			$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_FULL_SPECIAL_CHARS);
			$details = nl2br(filter_input(INPUT_POST, 'details', FILTER_SANITIZE_FULL_SPECIAL_CHARS));

			if (empty($name) OR empty($details))
			{
				$errormsg = "All fields are required";
			}
			else
			{
				if ($_POST['submit'] == "Add Entry") 
				{
				$query = $pdo->prepare('INSERT INTO candidates (name, details, entrydatetime) VALUES (? ? ?)');
				$query->bindValue(1, $name);
				$query->bindValue(2, $details);
				$query->bindValue(3, time());

				$query->execute();

				header('Location: index.php');
				}
				
			}
		}

		?>

		<!DOCTYPE html>
		<html lang="en">
		<head>
			<title>Candidate Scheduler CMS</title>
			<link rel="stylesheet" type="text/css" href="../stylescss/style.css">
		</head>
		<body>
			<div class="container">
				<a href="index.php" id="logo">
					Candidate Scheduler
				</a>

				<br>

				<h3>
					Add Entry
				</h3>

				<?php if (isset($errormsg)) { ?>
					<small style="color: red;">
						<?php echo $errormsg; ?>
					</small>
				<?php } ?>
				
				<form action="add.php" method="POST">
					<input type="text" name="name" placeholder="Candidate Name" />
					<br>
					<br>
					<textarea rows="20" cols="60" placeholder="Details" name="details"></textarea>
					<br><br>
					<input type="submit" value="Add Entry" name="submit">
					
				</form>

			</div>

		</body>
		</html>


		<?php


	}
	else
	{
		header('Location: index.php');
	}



?>