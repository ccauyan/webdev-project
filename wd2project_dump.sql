-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2020 at 04:00 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wd2project`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `candidateid` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `entrydatetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`candidateid`, `name`, `details`, `entrydatetime`) VALUES
(1, 'Carlo Cauyan', 'Applying for IT Manager', 1587053666),
(2, 'cat', 'applying for a cat\r\nresume to follow', 1587130824),
(3, 'Nonie Chan', 'No particular position applied', 1587130840),
(4, 'Dog Dog', 'Applying for dog', 1587130905),
(5, 'carlo carlo', 'no position in particular', 1587130930),
(6, 'mike mike', 'no position applied\r\n\r\n\r\nyet', 1587130974),
(7, 'allan allan', 'applying for manager', 1587131017),
(8, 'user user', 'test user', 1587131019),
(9, 'test', 'test', 1587131050),
(10, 'carlo cauyan', 'test person', 1587131092),
(11, 'bat', 'applying for a co-op position', 1587584797),
(12, 'bat', 'applying for temp position', 1587584869);

-- --------------------------------------------------------

--
-- Table structure for table `interviewsched`
--

CREATE TABLE `interviewsched` (
  `scheduleid` int(10) NOT NULL,
  `candidateid` int(10) NOT NULL,
  `interviewer` varchar(50) NOT NULL,
  `position` varchar(30) NOT NULL,
  `interviewdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `joboffers`
--

CREATE TABLE `joboffers` (
  `jobofferid` int(10) NOT NULL,
  `position` varchar(30) NOT NULL,
  `salary` decimal(30,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userlogins`
--

CREATE TABLE `userlogins` (
  `loginid` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `accesslevel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogins`
--

INSERT INTO `userlogins` (`loginid`, `username`, `password`, `accesslevel`) VALUES
(1, 'admin', 'af88a0ae641589b908fa8b31f0fcf6e1', 'administrator'),
(2, 'generaluser1', 'af88a0ae641589b908fa8b31f0fcf6e1', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `userpassword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `userpassword`) VALUES
(1, 'admin', 'af88a0ae641589b908fa8b31f0fcf6e1'),
(2, 'generaluser1', 'af88a0ae641589b908fa8b31f0fcf6e1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`candidateid`);

--
-- Indexes for table `interviewsched`
--
ALTER TABLE `interviewsched`
  ADD PRIMARY KEY (`scheduleid`),
  ADD KEY `candidateforeign` (`candidateid`);

--
-- Indexes for table `joboffers`
--
ALTER TABLE `joboffers`
  ADD PRIMARY KEY (`jobofferid`);

--
-- Indexes for table `userlogins`
--
ALTER TABLE `userlogins`
  ADD PRIMARY KEY (`loginid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `candidateid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `interviewsched`
--
ALTER TABLE `interviewsched`
  MODIFY `scheduleid` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `joboffers`
--
ALTER TABLE `joboffers`
  MODIFY `jobofferid` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userlogins`
--
ALTER TABLE `userlogins`
  MODIFY `loginid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `interviewsched`
--
ALTER TABLE `interviewsched`
  ADD CONSTRAINT `candidateforeign` FOREIGN KEY (`candidateid`) REFERENCES `candidates` (`candidateid`) ON DELETE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
