<?php

	include_once('includephps/dbconnect.php');
	include_once('includephps/entry.php');

	$entry = new Entry;

	if (isset($_GET['id'])) 
	{
		// this displays the specific candidate
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$data = $entry->fetch_Specific_Data($id);
		
		?>

			<!DOCTYPE html>
			<html lang="en">
			<head>
				<title>Candidate Scheduler CMS</title>
				<link rel="stylesheet" type="text/css" href="stylescss/style.css">
			</head>
			<body>
				<div class="container">
					<a href="index.php" id="logo">
						Candidate Scheduler
					</a>

					<h3><?php echo $data['name']; ?> - 
						<small>
							posted on <?php echo date('l jS', $data['entrydatetime']) ?>
						</small>
					</h3>

					<p>
						<?php echo $data['details']; ?>
					</p>

					<a href="index.php">&larr; Go Back</a>

				</div>

			</body>
			</html>

		<?php 
	}

	else
	{
		// goes back to index.php
		header('Location: index.php');
		exit();
	}

?>