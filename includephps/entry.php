<?php 
	
	/**
	 * Set of methods/functions to fetch data for other php files
	 * In this way, only the methods will be called (similar to Programming 1 and 2)
	 */
	class Entry 
	{
		
		public function fetch_All()
		{
			global $pdo;

			$query = $pdo->prepare("SELECT * FROM candidates");
			$query->execute();

			return $query->fetchAll();

		}

		public function fetch_Specific_Data($candidateid)
		{
			// for fetching specific data defined by get parameter
			global $pdo;

			$query = $pdo->prepare("SELECT * FROM candidates WHERE candidateid = ?");
			$query->bindValue(1, $candidateid, PDO::PARAM_INT);
			$query->execute();

			return $query->fetch();
		}
	}

?>