<?php

	include_once('includephps/dbconnect.php');
	include_once('includephps/entry.php');

	$entry = new Entry;
	$entries = $entry->fetch_All();


	echo time();




?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Candidate Scheduler CMS</title>
	<link rel="stylesheet" type="text/css" href="stylescss/style.css">
</head>
<body>
	<div class="container">
		<a href="index.php" id="logo">
			Candidate Scheduler
		</a>

		<ol>
			<?php foreach ($entries as $entry) { ?>
				<li>
					<a href="entry.php?id=<?php echo $entry['candidateid']; ?>">
						<?php echo $entry['name']; ?>
					</a>
					<small>
						- posted on <?php echo date('l jS', $entry['entrydatetime']); ?>
					</small>
				</li>
			<?php } ?>
			
		</ol>

		<br />
		<br />
		<small><a href="adminphp">Admin Login</a></small>
		<br>
		<br>
		<br>
		<small><a href="searchphp">Search Something</a></small>
	</div>

</body>
</html>